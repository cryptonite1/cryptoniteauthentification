package fr.isika.al9.userRegistration.email;

public interface EmailSender  {
	
	void send(String to, String email);

}
