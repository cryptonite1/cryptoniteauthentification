package fr.isika.al9.userRegistration.appuser;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import fr.isika.al9.userRegistration.registration.RegistrationRequest;
import fr.isika.al9.userRegistration.registration.token.ConfirmationToken;
import fr.isika.al9.userRegistration.registration.token.ConfirmationTokenService;
import lombok.AllArgsConstructor;
import lombok.Data;

@Service
@AllArgsConstructor
@Data
public class AppUserService implements UserDetailsService{
	
	private final static String USER_NOT_FOUND_MSG = "L'utilisateur avec l'email %s est introuvable !";
	
	@Autowired
	private final AppUserRepository appUserRepository;
	
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	
	private final ConfirmationTokenService confirmationTokenService;


	/*@Override
	@Transactional
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return appUserRepository.findByEmail(email)
				.orElseThrow(() -> new UsernameNotFoundException(
						String.format(USER_NOT_FOUND_MSG, email)));
	}*/
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser user = appUserRepository.getByEmail(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}
	
	public AppUser save(RegistrationRequest user) {
		AppUser newUser = new AppUser();
		newUser.setEmail(user.getEmail());
		newUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		return appUserRepository.save(newUser);
	}
	
	public String delete(String email) {
		AppUser user = appUserRepository.getByEmail(email);
		appUserRepository.delete(user);
		return "utilisateur " + user.getLastName() + "supprimé"; 	
	}

	
	public String signUpUser(AppUser request) {
		boolean userExists = appUserRepository.
				findByEmail(request.getEmail())
				.isPresent();
		
		
		
		if (userExists) {
			// verifier que les attributs sont les meme 
			// si enable est false, renvoyer un mail de confirmation
			
			throw new IllegalStateException("L'email est déjà pris");
		}
		
		String encodedPassword = bCryptPasswordEncoder
				.encode(request.getPassword());
		
		request.setPassword(encodedPassword);
		System.out.println(request);
		
		appUserRepository.save(request);
		
		String token = UUID.randomUUID().toString();
		ConfirmationToken confirmationToken = new ConfirmationToken(
				token,
				LocalDateTime.now(),
				LocalDateTime.now().plusMinutes(15),
				request
				);
		
		confirmationTokenService.saveConfirmationToken(confirmationToken);
		
		
		return token;	}
	
	
	public int enableAppUser(String email) {
        return appUserRepository.enableAppUser(email);
    }
	

}
