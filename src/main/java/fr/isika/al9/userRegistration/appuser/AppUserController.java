package fr.isika.al9.userRegistration.appuser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import fr.isika.al9.userRegistration.security.config.JwtTokenUtil;
import fr.isika.al9.userRegistration.security.config.model.JwtRequest;
import fr.isika.al9.userRegistration.security.config.model.JwtResponse;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/cryptonite")
@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor
public class AppUserController {
	
	
	private final AppUserService appUserService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private AppUserRepository app;
	
	@GetMapping(path = "/user")
	public String welcomeUser() {
		return "Bienvenue sur notre Api Utilsateur Cryptonite";
	}
	
	
	/*@GetMapping(path = "/admin")
	public String welcomeAdmin() {
		return "Bienvenue sur notre Api Admin Cryptonite";
	}*/
	
	@PostMapping(value = "/login")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
		
		Logger logger = LoggerFactory.getLogger(authenticationRequest.getEmail());
		authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
		

		final UserDetails userDetails =	appUserService.loadUserByUsername(authenticationRequest.getEmail());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(token));
	}
	
	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	
	@GetMapping(value = "/getUser/{email}") 
	public AppUser getUser(@PathVariable String email) {
		AppUser user  = app.getByEmail(email);
		return user;
		
	}
	
	@PostMapping(value = "/postPicture/{email}/{picture}")
	public void postPicture(@PathVariable String email, @PathVariable String picture) {
		AppUser user  = app.getByEmail(email);
		user.setPicture(picture);
		app.save(user);
	}
		

}
