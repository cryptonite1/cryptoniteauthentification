package fr.isika.al9.userRegistration.security.config.model;

import java.io.Serializable;

public class JwtRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2160169561177239866L;
	
	private String email;
	private String password;
	
	//default constructor for JSON Parsing
	public JwtRequest()
	{
	}

	public JwtRequest(String email, String password) {
		this.setEmail(email);
		this.setPassword(password);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}


