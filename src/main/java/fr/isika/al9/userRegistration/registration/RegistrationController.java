package fr.isika.al9.userRegistration.registration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/cryptonite")
@AllArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class RegistrationController {

	private final RegistrationService registrationService;
	
	
	@PostMapping(value = "/register")
	public String register(@RequestBody RegistrationRequest request) {
		Logger logger = LoggerFactory.getLogger(this.getClass());
		logger.info("Register : **" + request);
		return registrationService.register(request);
	}
	
	
	@GetMapping(path = "/confirm")
	public String confirm(@RequestParam("token") String token) {
		return registrationService.confirmToken(token);
	}
	
}
