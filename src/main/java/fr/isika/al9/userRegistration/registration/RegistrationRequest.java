package fr.isika.al9.userRegistration.registration;

import lombok.AllArgsConstructor;
import lombok.Data;


@AllArgsConstructor
@Data
public class RegistrationRequest {

	private final String firstname;
	private final String lastname;
	private final String email;
	private final String password;
}
