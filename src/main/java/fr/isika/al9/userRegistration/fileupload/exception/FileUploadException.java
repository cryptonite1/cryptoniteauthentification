package fr.isika.al9.userRegistration.fileupload.exception;

public class FileUploadException extends RuntimeException {
	
	public FileUploadException(String msg) {
        super(msg);
    }

}
